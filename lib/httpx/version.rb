# frozen_string_literal: true

module HTTPX
  VERSION = "1.2.1"
end
